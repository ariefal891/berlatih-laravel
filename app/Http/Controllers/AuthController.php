<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }
    public function signup(Request $request)
    {
        // dd($request->all());
        $nama_depan = $request['nama_depan'];
        $nama_belakang = $request['nama_belakang'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $ls = $request ['ls'];
        $bio = $request['bio'];

        return view('page.welcome', compact("nama_depan", "nama_belakang", "gender", "nationality", "ls", "bio"));
    }
}