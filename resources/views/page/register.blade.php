@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/signup" method="POST">
        @csrf
        <!-- Code untuk input type text -->
        <label for="nama_depan">First Name :</label> <br>
        <input type="text" name="nama_depan"><br> 
        <label for="nama_belakang">Last Name :</label> <br> 
        <input type="text" name="nama_belakang"> <br> <br>
        <!-- Code untuk input type text -->
        <!-- Code untuk input type radio -->
        <label for="gender">Gender</label> <br>
        <input type="radio" name="gender" value="Male"> Male <br>
        <input type="radio" name="gender" value="Female"> Female <br> <br>
        <!-- Code untuk input type radio -->
        <!-- Code untuk input type select -->
        <label >Nationality</label> <br> 
        <select name="nationality" id="">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
            <option value="4">Palestina</option>
        </select> <br> <br>
        <!-- Code untuk input type select -->
        <!-- Code untuk input type checkbox -->
        <label for="ls"> Language Spoken</label> <br> 
        <input type="checkbox" name="ls"> Bahasa Indonesia <br>
        <input type="checkbox" name="ls"> English <br>
        <input type="checkbox" name="ls"> Other <br> <br>
        <!-- Code untuk input type checkbox -->
        <!-- Code untuk input type textarea -->
        <label >Bio</label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <!-- Code untuk input type textarea -->
        <!-- Code untuk input type submit -->
        <input type="submit" value="Sign Up"><br><br>
        <!-- Code untuk input type submit -->
    </form>
    <a href="/">< Kembali ke Home</a>
@endsection