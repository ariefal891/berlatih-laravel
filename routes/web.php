<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\IndexController;

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@register');
Route::post('/signup', 'AuthController@signup');

Route::get('/data-table', 'IndexController@table');

// CRUD Tabel Cast
//Create Data
Route::get('/cast/create', 'CastController@create');//mengarah ke form tambah cast
Route::post('/cast', 'CastController@store');//untuk menyimpan data from cast ke database table cast

//Read Data
Route::get('/cast', 'CastController@index');//ambil data ke database dan ditampilkan di blade
Route::get('/cast/{cast_id}', 'CastController@show'); //Route Detai Cast

//Update Data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');//Edit Data
Route::put('/cast/{cast_id}', 'CastController@update');//update data

//Delete data
Route::delete('/cast/{cast_id}', 'CastController@destroy');//Delete data